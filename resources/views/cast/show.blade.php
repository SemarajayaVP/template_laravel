@extends('layout.master')
@section('title','Show Detail ')
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Show Cast Detail</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Cast Detail</li>
                    </ol>
                </div><!-- /.col -->
            </div>
        </div>
    </div>
 <!-- End Content Header (Page header) -->
 <!-- Main content -->
 <section class="content">
    <div class="container-fluid">
        <div class="row">
           <div class="col-lg-12">
               <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Cast Show By Id {{$castById->id}}</h3>

            </div>
              <!-- /.card-header -->
              <div class="card-body">
                  <div class="col-md-3">
                        <a href={{route('Cast.index')}} class="btn btn-success">  Back</a>
                    </div>
                  <h1> Nama : {{$castById->nama}}</h1>
                  <h1> Umur : {{$castById->umur}}</h1>
                  <h4> Bio : {{$castById->umur}}</p>
              </div>
            </div>
           </div>
        </div>
    </div>
 </section>
@endsection
